<?php

use Illuminate\Database\Seeder;

class StockSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Stock::create([
           'id' => 1,
            'quantity' => 100
        ]);

        \App\Stock::create([
            'id' => 2,
            'quantity' => 120
        ]);

        \App\Stock::create([
            'id' => 3,
            'quantity' => 90
        ]);
    }
}
