<?php

use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Product::create([
            'id' => 1,
            'stock_id' => 1,
            'name' => 'Chocolate Shot'
        ]);

        \App\Product::create([
            'id' => 2,
            'stock_id' => 2,
            'name' => 'Chocolate Block'
        ]);

        \App\Product::create([
            'id' => 3,
            'stock_id' => 3,
            'name' => 'Toblerone'
        ]);
    }
}
