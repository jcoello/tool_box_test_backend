<?php

namespace App\Http\Controllers;

use App\Product;
use App\Stock;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class StockController extends Controller
{
    public function index($productId)
    {
        return DB::table('stocks')->join('products', 'stocks.id', '=', 'products.stock_id')
            ->where('products.id', '=', $productId)->get();
    }

    public function store()
    {
        Stock::create(Input::all());
    }

    public function destroy($productId, $stockId)
    {
        Product::findOrFail($productId)->delete();
        Stock::findOrFail($stockId)->delete();
    }
}
